from django.db import models

class Equipo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    miembros = models.CharField(max_length=100, verbose_name='Miembros')

    def __str__(self):
        fila = "Nombre: " + self.nombre + " - " + "Miembros: " + self.miembros
        return fila

   